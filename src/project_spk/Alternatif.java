/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project_spk;

/**
 *
 * @author Joice
 */
public class Alternatif {
    double x11, x12, x13,
           y21, y22, y23,
           z31, z32, z33;
    
    double a11, a12, a13, a14,
           b21, b22, b23, b24,
           c31, c32, c33, c34,
           d41, d42, d43, d44;
    
    double iterasi1_11, iterasi1_12, iterasi1_13,
           iterasi1_21, iterasi1_22, iterasi1_23,
           iterasi1_31, iterasi1_32, iterasi1_33;
    double iterasi1_jumlahBaris1, iterasi1_jumlahBaris2, iterasi1_jumlahBaris3, totalBaris,
            iterasi1_eigenBaris1, iterasi1_eigenBaris2, iterasi1_eigenBaris3;
    
    double iterasi2_11, iterasi2_12, iterasi2_13,
           iterasi2_21, iterasi2_22, iterasi2_23,
           iterasi2_31, iterasi2_32, iterasi2_33;
    double iterasi2_jumlahBaris1, iterasi2_jumlahBaris2, iterasi2_jumlahBaris3, totalBaris2,
            iterasi2_eigenBaris1, iterasi2_eigenBaris2, iterasi2_eigenBaris3;
    
    double iterasi3_11, iterasi3_12, iterasi3_13,
           iterasi3_21, iterasi3_22, iterasi3_23,
           iterasi3_31, iterasi3_32, iterasi3_33;
    double iterasi3_jumlahBaris1, iterasi3_jumlahBaris2, iterasi3_jumlahBaris3, totalBaris3,
            iterasi3_eigenBaris1, iterasi3_eigenBaris2, iterasi3_eigenBaris3;
    
    double CRi2, CIi2, CRi3, CIi3;
    
    double selisihNormalisasi_i1i2_baris1, selisihNormalisasi_i1i2_baris2, selisihNormalisasi_i1i2_baris3;
    
    double kriteria1_11, kriteria1_12, kriteria1_13, kriteria1_14,
           kriteria1_21, kriteria1_22, kriteria1_23, kriteria1_24,
           kriteria1_31, kriteria1_32, kriteria1_33, kriteria1_34,
           kriteria1_41, kriteria1_42, kriteria1_43, kriteria1_44;
    double kriteria1_jumlahBaris1, kriteria1_jumlahBaris2, kriteria1_jumlahBaris3, kriteria1_jumlahBaris4, totalBarisK1,
            kriteria1_eigenBaris1, kriteria1_eigenBaris2, kriteria1_eigenBaris3, kriteria1_eigenBaris4;
    
    double kriteria2_11, kriteria2_12, kriteria2_13, kriteria2_14,
           kriteria2_21, kriteria2_22, kriteria2_23, kriteria2_24,
           kriteria2_31, kriteria2_32, kriteria2_33, kriteria2_34,
           kriteria2_41, kriteria2_42, kriteria2_43, kriteria2_44;
    double kriteria2_jumlahBaris1, kriteria2_jumlahBaris2, kriteria2_jumlahBaris3, kriteria2_jumlahBaris4, totalBarisK2,
            kriteria2_eigenBaris1, kriteria2_eigenBaris2, kriteria2_eigenBaris3, kriteria2_eigenBaris4;
    
    double kriteria3_11, kriteria3_12, kriteria3_13, kriteria3_14,
           kriteria3_21, kriteria3_22, kriteria3_23, kriteria3_24,
           kriteria3_31, kriteria3_32, kriteria3_33, kriteria3_34,
           kriteria3_41, kriteria3_42, kriteria3_43, kriteria3_44;
    double kriteria3_jumlahBaris1, kriteria3_jumlahBaris2, kriteria3_jumlahBaris3, kriteria3_jumlahBaris4, totalBarisK3,
            kriteria3_eigenBaris1, kriteria3_eigenBaris2, kriteria3_eigenBaris3, kriteria3_eigenBaris4;
    
    double CRk2, CIk2, CRk3, CIk3;
    
    public Alternatif()
    {
        
    }
    public Alternatif(double thisX11, double thisX12, double thisX13,
                      double thisY21, double thisY22, double thisY23,
                      double thisZ31, double thisZ32, double thisZ33)
    {
        this.x11 = thisX11;
        this.x12 = thisX12;
        this.x13 = thisX13;
        this.y21 = thisY21;
        this.y22 = thisY22;
        this.y23 = thisY23;
        this.z31 = thisZ31;
        this.z32 = thisZ32;
        this.z33 = thisZ33;
    }
    
    public String iterasi1(String a, double thisX11, double thisX12, double thisX13,
                         double thisY21, double thisY22, double thisY23,
                         double thisZ31, double thisZ32, double thisZ33)
    {
        x11 = thisX11;
        x12 = thisX12;
        x13 = thisX13;
        y21 = thisY21;
        y22 = thisY22;
        y23 = thisY23;
        z31 = thisZ31;
        z32 = thisZ32;
        z33 = thisZ33;
        
        iterasi1_11 = (thisX11*thisX11) + (thisX12*thisY21) + (thisX13*thisZ31);
        iterasi1_12 = (thisX11*thisX12) + (thisX12*thisY22) + (thisX13*thisZ32);
        iterasi1_13 = (thisX11*thisX13) + (thisX12*thisY23) + (thisX13*thisZ33);
        iterasi1_21 = (thisY21*thisX11) + (thisY22*thisY21) + (thisY23*thisZ31);
        iterasi1_22 = (thisY21*thisX12) + (thisY22*thisY22) + (thisY23*thisZ32);
        iterasi1_23 = (thisY21*thisX13) + (thisY22*thisY23) + (thisY23*thisZ33);
        iterasi1_31 = (thisZ31*thisX11) + (thisZ32*thisY21) + (thisZ33*thisZ31);
        iterasi1_32 = (thisZ31*thisX12) + (thisZ32*thisY22) + (thisZ33*thisZ32);
        iterasi1_33 = (thisZ31*thisX13) + (thisZ32*thisY23) + (thisZ33*thisZ33);
        
        iterasi1_jumlahBaris1 = iterasi1_11 + iterasi1_12 + iterasi1_13;
        iterasi1_jumlahBaris2 = iterasi1_21 + iterasi1_22 + iterasi1_23;
        iterasi1_jumlahBaris3 = iterasi1_31 + iterasi1_32 + iterasi1_33;
        totalBaris = iterasi1_jumlahBaris1 + iterasi1_jumlahBaris2 + iterasi1_jumlahBaris3;
        
        iterasi1_eigenBaris1 = iterasi1_jumlahBaris1 / totalBaris;
        iterasi1_eigenBaris2 = iterasi1_jumlahBaris2 / totalBaris;
        iterasi1_eigenBaris3 = iterasi1_jumlahBaris3 / totalBaris;
        
        return a;
    }
    
    public double[][] tampilIterasi1()
    {
        double[][] tampilI1 = new double[3][3];
        tampilI1[0][0] = iterasi1_11;
        tampilI1[0][1] = iterasi1_12;
        tampilI1[0][2] = iterasi1_13;
        tampilI1[1][0] = iterasi1_21;
        tampilI1[1][1] = iterasi1_22;
        tampilI1[1][2] = iterasi1_23;
        tampilI1[2][0] = iterasi1_31;
        tampilI1[2][1] = iterasi1_32;
        tampilI1[2][2] = iterasi1_33;
        
        return tampilI1;
    }
    public double[] tampilJumBarisI1()
    {
        double[] a = new double[3];
        a[0] = iterasi1_jumlahBaris1;
        a[1] = iterasi1_jumlahBaris2;
        a[2] = iterasi1_jumlahBaris3;
        return a;
    }
    public double[] tampilNormI1()
    {
        double[] a = new double[3];
        a[0] = iterasi1_eigenBaris1;
        a[1] = iterasi1_eigenBaris2;
        a[2] = iterasi1_eigenBaris3;
        return a;
    }
    
    public String[] iterasi2(String a)
    {
        String[] syarat = new String[5] ;
        iterasi2_11 = (iterasi1_11*iterasi1_11) + (iterasi1_12*iterasi1_21) + (iterasi1_13*iterasi1_31);
        iterasi2_12 = (iterasi1_11*iterasi1_12) + (iterasi1_12*iterasi1_22) + (iterasi1_13*iterasi1_32);
        iterasi2_13 = (iterasi1_11*iterasi1_13) + (iterasi1_12*iterasi1_23) + (iterasi1_13*iterasi1_33);
        iterasi2_21 = (iterasi1_21*iterasi1_11) + (iterasi1_22*iterasi1_21) + (iterasi1_23*iterasi1_31);
        iterasi2_22 = (iterasi1_21*iterasi1_12) + (iterasi1_22*iterasi1_22) + (iterasi1_23*iterasi1_32);
        iterasi2_23 = (iterasi1_21*iterasi1_13) + (iterasi1_22*iterasi1_23) + (iterasi1_23*iterasi1_33);
        iterasi2_31 = (iterasi1_31*iterasi1_11) + (iterasi1_32*iterasi1_21) + (iterasi1_33*iterasi1_31);
        iterasi2_32 = (iterasi1_31*iterasi1_12) + (iterasi1_32*iterasi1_22) + (iterasi1_33*iterasi1_32);
        iterasi2_33 = (iterasi1_31*iterasi1_13) + (iterasi1_32*iterasi1_23) + (iterasi1_33*iterasi1_33);
         
        iterasi2_jumlahBaris1 = iterasi2_11 + iterasi2_12 + iterasi2_13;
        iterasi2_jumlahBaris2 = iterasi2_21 + iterasi2_22 + iterasi2_23;
        iterasi2_jumlahBaris3 = iterasi2_31 + iterasi2_32 + iterasi2_33;
        totalBaris2 = iterasi2_jumlahBaris1 + iterasi2_jumlahBaris2 + iterasi2_jumlahBaris3;
        
        iterasi2_eigenBaris1 = iterasi2_jumlahBaris1 / totalBaris2;
        iterasi2_eigenBaris2 = iterasi2_jumlahBaris2 / totalBaris2;
        iterasi2_eigenBaris3 = iterasi2_jumlahBaris3 / totalBaris2;
        
        selisihNormalisasi_i1i2_baris1 = Math.abs(iterasi2_eigenBaris1 - iterasi1_eigenBaris1);
        selisihNormalisasi_i1i2_baris2 = Math.abs(iterasi2_eigenBaris2 - iterasi1_eigenBaris2);
        selisihNormalisasi_i1i2_baris3 = Math.abs(iterasi2_eigenBaris3 - iterasi1_eigenBaris3);
        
        if(selisihNormalisasi_i1i2_baris1 > 0.0100 ||
           selisihNormalisasi_i1i2_baris2 > 0.0100 ||
           selisihNormalisasi_i1i2_baris3 > 0.0100)
        {
            iterasi3_11 = (iterasi2_11*iterasi2_11) + (iterasi2_12*iterasi2_21) + (iterasi2_13*iterasi2_31);
            iterasi3_12 = (iterasi2_11*iterasi2_12) + (iterasi2_12*iterasi2_22) + (iterasi2_13*iterasi2_32);
            iterasi3_13 = (iterasi2_11*iterasi2_13) + (iterasi2_12*iterasi2_23) + (iterasi2_13*iterasi2_33);
            iterasi3_21 = (iterasi2_21*iterasi2_11) + (iterasi2_22*iterasi2_21) + (iterasi2_23*iterasi2_31);
            iterasi3_22 = (iterasi2_21*iterasi2_12) + (iterasi2_22*iterasi2_22) + (iterasi2_23*iterasi2_32);
            iterasi3_23 = (iterasi2_21*iterasi2_13) + (iterasi2_22*iterasi2_23) + (iterasi2_23*iterasi2_33);
            iterasi3_31 = (iterasi2_31*iterasi2_11) + (iterasi2_32*iterasi2_21) + (iterasi2_33*iterasi2_31);
            iterasi3_32 = (iterasi2_31*iterasi2_12) + (iterasi2_32*iterasi2_22) + (iterasi2_33*iterasi2_32);
            iterasi3_33 = (iterasi2_31*iterasi2_13) + (iterasi2_32*iterasi2_23) + (iterasi2_33*iterasi2_33);
            
            iterasi3_jumlahBaris1 = iterasi3_11 + iterasi3_12 + iterasi3_13;
            iterasi3_jumlahBaris2 = iterasi3_21 + iterasi3_22 + iterasi3_23;
            iterasi3_jumlahBaris3 = iterasi3_31 + iterasi3_32 + iterasi3_33;
            totalBaris3 = iterasi3_jumlahBaris1 + iterasi3_jumlahBaris2 + iterasi3_jumlahBaris3;

            iterasi3_eigenBaris1 = iterasi3_jumlahBaris1 / totalBaris3;
            iterasi3_eigenBaris2 = iterasi3_jumlahBaris2 / totalBaris3;
            iterasi3_eigenBaris3 = iterasi3_jumlahBaris3 / totalBaris3;
            
            double selisihNormalisasi3_baris1 = Math.abs(iterasi3_eigenBaris1 - iterasi2_eigenBaris1);
            double selisihNormalisasi3_baris2 = Math.abs(iterasi3_eigenBaris2 - iterasi2_eigenBaris2);
            double selisihNormalisasi3_baris3 = Math.abs(iterasi3_eigenBaris3 - iterasi2_eigenBaris3);
            
            double lambdaMax_baris1 = (x11 + y21 + z31) * iterasi3_eigenBaris1;
            double lambdaMax_baris2 = (x12 + y22 + z32) * iterasi3_eigenBaris2;
            double lambdaMax_baris3 = (x13 + y23 + z33) * iterasi3_eigenBaris3;
            double totalLambdaMax = lambdaMax_baris1 + lambdaMax_baris2 + lambdaMax_baris3;
            
            CIi3 = (totalLambdaMax-3) / 2.0;
            CRi3 = CIi3 / 0.58;
            if(CRi3 <= 0.1000)
            {
                syarat[0] = "Berhenti di iterasi 3";
                syarat[1] = "Judgement " +a+ " dapat diterima";
                syarat[2] = selisihNormalisasi3_baris1 + "";
                syarat[3] = selisihNormalisasi3_baris2 + "";
                syarat[4] = selisihNormalisasi3_baris3 + "";
            }
            else if (CRi3 > 0.1000)
            {
                syarat[0] = "Berhenti di iterasi 3";
                syarat[1] = "Judgement " +a+ " tidak dapat diterima";
                syarat[2] = 0 + "";
                syarat[3] = 0 + "";
                syarat[4] = 0 + "";
            }
        }
        else if(selisihNormalisasi_i1i2_baris1 <= 0.0100 &&
                selisihNormalisasi_i1i2_baris2 <= 0.0100 &&
                selisihNormalisasi_i1i2_baris3 <= 0.0100)
        {
            double lambdaMax_baris1 = (x11 + y21 + z31) * iterasi2_eigenBaris1;
            double lambdaMax_baris2 = (x12 + y22 + z32) * iterasi2_eigenBaris2;
            double lambdaMax_baris3 = (x13 + y23 + z33) * iterasi2_eigenBaris3;
            double totalLambdaMax = lambdaMax_baris1 + lambdaMax_baris2 + lambdaMax_baris3;
            
            CIi2 = (totalLambdaMax-3) / 2.0;
            CRi2 = CIi2 / 0.58;
            if(CRi2 <= 0.1000)
            {
                syarat[0] = "Berhenti di iterasi 2";
                syarat[1] = "Judgement " +a+ " dapat diterima";
                syarat[2] = selisihNormalisasi_i1i2_baris1 + "";
                syarat[3] = selisihNormalisasi_i1i2_baris2 + "";
                syarat[4] = selisihNormalisasi_i1i2_baris3 + "";
            }
            else if (CRi2 > 0.1000)
            {
                syarat[0] = "Berhenti di iterasi 2";
                syarat[1] = "Judgement " +a+ " tidak dapat diterima";
                syarat[2] = 0 + "";
                syarat[3] = 0 + "";
                syarat[4] = 0 + "";
            }
        }
        
        return syarat;
    }
    
    public double[][] tampilIterasi2()
    {
        double[][] tampilI2 = new double[3][3];
        tampilI2[0][0] = iterasi2_11;
        tampilI2[0][1] = iterasi2_12;
        tampilI2[0][2] = iterasi2_13;
        tampilI2[1][0] = iterasi2_21;
        tampilI2[1][1] = iterasi2_22;
        tampilI2[1][2] = iterasi2_23;
        tampilI2[2][0] = iterasi2_31;
        tampilI2[2][1] = iterasi2_32;
        tampilI2[2][2] = iterasi2_33;
        
        return tampilI2;
    }
    public double[] tampilJumBarisI2()
    {
         double[] a = new double[3];
         a[0] = iterasi2_jumlahBaris1;
         a[1] = iterasi2_jumlahBaris2;
         a[2] = iterasi2_jumlahBaris3;
         return a;
    }
    public double[] tampilNormI2()
    {
        double[] a = new double[3];
        a[0] = iterasi2_eigenBaris1;
        a[1] = iterasi2_eigenBaris2;
        a[2] = iterasi2_eigenBaris3;
        return a;
    }
    public double[][] tampilIterasi3()
    {
        double[][] tampilI3 = new double[3][3];
        tampilI3[0][0] = iterasi3_11;
        tampilI3[0][1] = iterasi3_12;
        tampilI3[0][2] = iterasi3_13;
        tampilI3[1][0] = iterasi3_21;
        tampilI3[1][1] = iterasi3_22;
        tampilI3[1][2] = iterasi3_23;
        tampilI3[2][0] = iterasi3_31;
        tampilI3[2][1] = iterasi3_32;
        tampilI3[2][2] = iterasi3_33;
        
        return tampilI3;
    }
    public double[] tampilJumBarisI3()
    {
         double[] a = new double[3];
         a[0] = iterasi3_jumlahBaris1;
         a[1] = iterasi3_jumlahBaris2;
         a[2] = iterasi3_jumlahBaris3;
         return a;
    }
    public double[] tampilNormI3()
    {
        double[] a = new double[3];
        a[0] = iterasi3_eigenBaris1;
        a[1] = iterasi3_eigenBaris2;
        a[2] = iterasi3_eigenBaris3;
        return a;
    }
    public double[] tampilCICRAlternatif()
    {
        double[] a = new double[4];
        a[0] = CIi2;
        a[1] = CRi2;
        a[2] = CIi3;
        a[3] = CRi3;
        return a;
    }
    
    public String kriteria1(String a, double thisA11, double thisA12, double thisA13, double thisA14,
                          double thisB21, double thisB22, double thisB23, double thisB24,
                          double thisC31, double thisC32, double thisC33, double thisC34,
                          double thisD41, double thisD42, double thisD43, double thisD44)
    {
        a11 = thisA11;
        a12 = thisA12;
        a13 = thisA13;
        a14 = thisA14;
        b21 = thisB21;
        b22 = thisB22;
        b23 = thisB23;
        b24 = thisB24;
        c31 = thisC31;
        c32 = thisC32;
        c33 = thisC33;
        c34 = thisC34;
        d41 = thisD41;
        d42 = thisD42;
        d43 = thisD43;
        d44 = thisD44;
        
        kriteria1_11 = (thisA11*thisA11) + (thisA12*thisB21) + (thisA13*thisC31) + (thisA14*thisD41);
        kriteria1_12 = (thisA11*thisA12) + (thisA12*thisB22) + (thisA13*thisC32) + (thisA14*thisD42);;
        kriteria1_13 = (thisA11*thisA13) + (thisA12*thisB23) + (thisA13*thisC33) + (thisA14*thisD43);;
        kriteria1_14 = (thisA11*thisA14) + (thisA12*thisB24) + (thisA13*thisC34) + (thisA14*thisD44);;
        kriteria1_21 = (thisB21*thisA11) + (thisB22*thisB21) + (thisB23*thisC31) + (thisB24*thisD41);;
        kriteria1_22 = (thisB21*thisA12) + (thisB22*thisB22) + (thisB23*thisC32) + (thisB24*thisD42);;
        kriteria1_23 = (thisB21*thisA13) + (thisB22*thisB23) + (thisB23*thisC33) + (thisB24*thisD43);;
        kriteria1_24 = (thisB21*thisA14) + (thisB22*thisB24) + (thisB23*thisC34) + (thisB24*thisD44);;
        kriteria1_31 = (thisC31*thisA11) + (thisC32*thisB21) + (thisC33*thisC31) + (thisC34*thisD41);;
        kriteria1_32 = (thisC31*thisA12) + (thisC32*thisB22) + (thisC33*thisC32) + (thisC34*thisD42);;
        kriteria1_33 = (thisC31*thisA13) + (thisC32*thisB23) + (thisC33*thisC33) + (thisC34*thisD43);;
        kriteria1_34 = (thisC31*thisA14) + (thisC32*thisB24) + (thisC33*thisC34) + (thisC34*thisD44);;
        kriteria1_41 = (thisD41*thisA11) + (thisD42*thisB21) + (thisD43*thisC31) + (thisD44*thisD41);;
        kriteria1_42 = (thisD41*thisA12) + (thisD42*thisB22) + (thisD43*thisC32) + (thisD44*thisD42);;
        kriteria1_43 = (thisD41*thisA13) + (thisD42*thisB23) + (thisD43*thisC33) + (thisD44*thisD43);;
        kriteria1_44 = (thisD41*thisA14) + (thisD42*thisB24) + (thisD43*thisC34) + (thisD44*thisD44);;
        
        kriteria1_jumlahBaris1 = kriteria1_11 + kriteria1_12 + kriteria1_13 + kriteria1_14;
        kriteria1_jumlahBaris2 = kriteria1_21 + kriteria1_22 + kriteria1_23 + kriteria1_24;
        kriteria1_jumlahBaris3 = kriteria1_31 + kriteria1_32 + kriteria1_33 + kriteria1_34;
        kriteria1_jumlahBaris4 = kriteria1_41 + kriteria1_42 + kriteria1_43 + kriteria1_44;
        totalBarisK1 = kriteria1_jumlahBaris1 + kriteria1_jumlahBaris2 + kriteria1_jumlahBaris3 + kriteria1_jumlahBaris4;
        
        kriteria1_eigenBaris1 = kriteria1_jumlahBaris1 / totalBarisK1;
        kriteria1_eigenBaris2 = kriteria1_jumlahBaris2 / totalBarisK1;
        kriteria1_eigenBaris3 = kriteria1_jumlahBaris3 / totalBarisK1;
        kriteria1_eigenBaris4 = kriteria1_jumlahBaris4 / totalBarisK1;
        
        return a;
    }
    
    public double[][] tampilKriteria1()
    {
        double[][] tampilK1 = new double[4][4];
        tampilK1[0][0] = kriteria1_11;
        tampilK1[0][1] = kriteria1_12;
        tampilK1[0][2] = kriteria1_13;
        tampilK1[0][3] = kriteria1_14;
        tampilK1[1][0] = kriteria1_21;
        tampilK1[1][1] = kriteria1_22;
        tampilK1[1][2] = kriteria1_23;
        tampilK1[1][3] = kriteria1_24;
        tampilK1[2][0] = kriteria1_31;
        tampilK1[2][1] = kriteria1_32;
        tampilK1[2][2] = kriteria1_33;
        tampilK1[2][3] = kriteria1_34;
        tampilK1[3][0] = kriteria1_41;
        tampilK1[3][1] = kriteria1_42;
        tampilK1[3][2] = kriteria1_43;
        tampilK1[3][3] = kriteria1_44;
        
        return tampilK1;
    }
    public double[] tampilJumBarisK1()
    {
        double[] a = new double[4];
        a[0] = kriteria1_jumlahBaris1;
        a[1] = kriteria1_jumlahBaris2;
        a[2] = kriteria1_jumlahBaris3;
        a[3] = kriteria1_jumlahBaris4;
        return a;
    }
    public double[] tampilNormK1()
    {
        double[] a = new double[4];
        a[0] = kriteria1_eigenBaris1;
        a[1] = kriteria1_eigenBaris2;
        a[2] = kriteria1_eigenBaris3;
        a[3] = kriteria1_eigenBaris4;
        return a;
    }
    
    public String[] kriteria2(String a)
    {
        String[] syarat = new String[6];
        kriteria2_11 = (kriteria1_11*kriteria1_11) + (kriteria1_12*kriteria1_21) + (kriteria1_13*kriteria1_31) + (kriteria1_14*kriteria1_41);
        kriteria2_12 = (kriteria1_11*kriteria1_12) + (kriteria1_12*kriteria1_22) + (kriteria1_13*kriteria1_32) + (kriteria1_14*kriteria1_42);;
        kriteria2_13 = (kriteria1_11*kriteria1_13) + (kriteria1_12*kriteria1_23) + (kriteria1_13*kriteria1_33) + (kriteria1_14*kriteria1_43);;
        kriteria2_14 = (kriteria1_11*kriteria1_14) + (kriteria1_12*kriteria1_24) + (kriteria1_13*kriteria1_34) + (kriteria1_14*kriteria1_44);;
        kriteria2_21 = (kriteria1_21*kriteria1_11) + (kriteria1_22*kriteria1_21) + (kriteria1_23*kriteria1_31) + (kriteria1_24*kriteria1_41);;
        kriteria2_22 = (kriteria1_21*kriteria1_12) + (kriteria1_22*kriteria1_22) + (kriteria1_23*kriteria1_32) + (kriteria1_24*kriteria1_42);;
        kriteria2_23 = (kriteria1_21*kriteria1_13) + (kriteria1_22*kriteria1_23) + (kriteria1_23*kriteria1_33) + (kriteria1_24*kriteria1_43);;
        kriteria2_24 = (kriteria1_21*kriteria1_14) + (kriteria1_22*kriteria1_24) + (kriteria1_23*kriteria1_34) + (kriteria1_24*kriteria1_44);;
        kriteria2_31 = (kriteria1_31*kriteria1_11) + (kriteria1_32*kriteria1_21) + (kriteria1_33*kriteria1_31) + (kriteria1_34*kriteria1_41);;
        kriteria2_32 = (kriteria1_31*kriteria1_12) + (kriteria1_32*kriteria1_22) + (kriteria1_33*kriteria1_32) + (kriteria1_34*kriteria1_42);;
        kriteria2_33 = (kriteria1_31*kriteria1_13) + (kriteria1_32*kriteria1_23) + (kriteria1_33*kriteria1_33) + (kriteria1_34*kriteria1_43);;
        kriteria2_34 = (kriteria1_31*kriteria1_14) + (kriteria1_32*kriteria1_24) + (kriteria1_33*kriteria1_34) + (kriteria1_34*kriteria1_44);;
        kriteria2_41 = (kriteria1_41*kriteria1_11) + (kriteria1_42*kriteria1_21) + (kriteria1_43*kriteria1_31) + (kriteria1_44*kriteria1_41);;
        kriteria2_42 = (kriteria1_41*kriteria1_12) + (kriteria1_42*kriteria1_22) + (kriteria1_43*kriteria1_32) + (kriteria1_44*kriteria1_42);;
        kriteria2_43 = (kriteria1_41*kriteria1_13) + (kriteria1_42*kriteria1_23) + (kriteria1_43*kriteria1_33) + (kriteria1_44*kriteria1_43);;
        kriteria2_44 = (kriteria1_41*kriteria1_14) + (kriteria1_42*kriteria1_24) + (kriteria1_43*kriteria1_34) + (kriteria1_44*kriteria1_44);;
         
        kriteria2_jumlahBaris1 = kriteria2_11 + kriteria2_12 + kriteria2_13 + kriteria2_14;
        kriteria2_jumlahBaris2 = kriteria2_21 + kriteria2_22 + kriteria2_23 + kriteria2_24;
        kriteria2_jumlahBaris3 = kriteria2_31 + kriteria2_32 + kriteria2_33 + kriteria2_34;
        kriteria2_jumlahBaris4 = kriteria2_41 + kriteria2_42 + kriteria2_43 + kriteria2_44;
        totalBarisK2 = kriteria2_jumlahBaris1 + kriteria2_jumlahBaris2 + kriteria2_jumlahBaris3 + kriteria2_jumlahBaris4;
        
        kriteria2_eigenBaris1 = kriteria2_jumlahBaris1 / totalBarisK2;
        kriteria2_eigenBaris2 = kriteria2_jumlahBaris2 / totalBarisK2;
        kriteria2_eigenBaris3 = kriteria2_jumlahBaris3 / totalBarisK2;
        kriteria2_eigenBaris4 = kriteria2_jumlahBaris4 / totalBarisK2;
        
        double selisihNormalisasi2_baris1 = Math.abs(kriteria2_eigenBaris1 - kriteria1_eigenBaris1);
        double selisihNormalisasi2_baris2 = Math.abs(kriteria2_eigenBaris2 - kriteria1_eigenBaris2);
        double selisihNormalisasi2_baris3 = Math.abs(kriteria2_eigenBaris3 - kriteria1_eigenBaris3);
        double selisihNormalisasi2_baris4 = Math.abs(kriteria2_eigenBaris4 - kriteria1_eigenBaris4);
        
        if(selisihNormalisasi2_baris1 > 0.0100 ||
           selisihNormalisasi2_baris2 > 0.0100 ||
           selisihNormalisasi2_baris3 > 0.0100 ||
           selisihNormalisasi2_baris4 > 0.0100)
        {
            kriteria3_11 = (kriteria2_11*kriteria2_11) + (kriteria2_12*kriteria2_21) + (kriteria2_13*kriteria2_31) + (kriteria2_14*kriteria2_41);
            kriteria3_12 = (kriteria2_11*kriteria2_12) + (kriteria2_12*kriteria2_22) + (kriteria2_13*kriteria2_32) + (kriteria2_14*kriteria2_42);;
            kriteria3_13 = (kriteria2_11*kriteria2_13) + (kriteria2_12*kriteria2_23) + (kriteria2_13*kriteria2_33) + (kriteria2_14*kriteria2_43);;
            kriteria3_14 = (kriteria2_11*kriteria2_14) + (kriteria2_12*kriteria2_24) + (kriteria2_13*kriteria2_34) + (kriteria2_14*kriteria2_44);;
            kriteria3_21 = (kriteria2_21*kriteria2_11) + (kriteria2_22*kriteria2_21) + (kriteria2_23*kriteria2_31) + (kriteria2_24*kriteria2_41);;
            kriteria3_22 = (kriteria2_21*kriteria2_12) + (kriteria2_22*kriteria2_22) + (kriteria2_23*kriteria2_32) + (kriteria2_24*kriteria2_42);;
            kriteria3_23 = (kriteria2_21*kriteria2_13) + (kriteria2_22*kriteria2_23) + (kriteria2_23*kriteria2_33) + (kriteria2_24*kriteria2_43);;
            kriteria3_24 = (kriteria2_21*kriteria2_14) + (kriteria2_22*kriteria2_24) + (kriteria2_23*kriteria2_34) + (kriteria2_24*kriteria2_44);;
            kriteria3_31 = (kriteria2_31*kriteria2_11) + (kriteria2_32*kriteria2_21) + (kriteria2_33*kriteria2_31) + (kriteria2_34*kriteria2_41);;
            kriteria3_32 = (kriteria2_31*kriteria2_12) + (kriteria2_32*kriteria2_22) + (kriteria2_33*kriteria2_32) + (kriteria2_34*kriteria2_42);;
            kriteria3_33 = (kriteria2_31*kriteria2_13) + (kriteria2_32*kriteria2_23) + (kriteria2_33*kriteria2_33) + (kriteria2_34*kriteria2_43);;
            kriteria3_34 = (kriteria2_31*kriteria2_14) + (kriteria2_32*kriteria2_24) + (kriteria2_33*kriteria2_34) + (kriteria2_34*kriteria2_44);;
            kriteria3_41 = (kriteria2_41*kriteria2_11) + (kriteria2_42*kriteria2_21) + (kriteria2_43*kriteria2_31) + (kriteria2_44*kriteria2_41);;
            kriteria3_42 = (kriteria2_41*kriteria2_12) + (kriteria2_42*kriteria2_22) + (kriteria2_43*kriteria2_32) + (kriteria2_44*kriteria2_42);;
            kriteria3_43 = (kriteria2_41*kriteria2_13) + (kriteria2_42*kriteria2_23) + (kriteria2_43*kriteria2_33) + (kriteria2_44*kriteria2_43);;
            kriteria3_44 = (kriteria2_41*kriteria2_14) + (kriteria2_42*kriteria2_24) + (kriteria2_43*kriteria2_34) + (kriteria2_44*kriteria2_44);;
            
            kriteria3_jumlahBaris1 = kriteria3_11 + kriteria3_12 + kriteria3_13 + kriteria3_14;
            kriteria3_jumlahBaris2 = kriteria3_21 + kriteria3_22 + kriteria3_23 + kriteria3_24;
            kriteria3_jumlahBaris3 = kriteria3_31 + kriteria3_32 + kriteria3_33 + kriteria3_34;
            kriteria3_jumlahBaris4 = kriteria3_41 + kriteria3_42 + kriteria3_43 + kriteria3_44;
            totalBarisK3 = kriteria3_jumlahBaris1 + kriteria3_jumlahBaris2 + kriteria3_jumlahBaris3 + kriteria3_jumlahBaris4;

            kriteria3_eigenBaris1 = iterasi3_jumlahBaris1 / totalBarisK3;
            kriteria3_eigenBaris2 = iterasi3_jumlahBaris2 / totalBarisK3;
            kriteria3_eigenBaris3 = iterasi3_jumlahBaris3 / totalBarisK3;
            kriteria3_eigenBaris4 = kriteria3_jumlahBaris4 / totalBarisK3;

            double selisihNormalisasi3_baris1 = Math.abs(kriteria3_eigenBaris1 - kriteria2_eigenBaris1);
            double selisihNormalisasi3_baris2 = Math.abs(kriteria3_eigenBaris2 - kriteria2_eigenBaris2);
            double selisihNormalisasi3_baris3 = Math.abs(kriteria3_eigenBaris3 - kriteria2_eigenBaris3);
            double selisihNormalisasi3_baris4 = Math.abs(kriteria3_eigenBaris4 - kriteria2_eigenBaris4);
            
            double lambdaMax_baris1 = (a11 + b21 + c31 + d41) * kriteria3_eigenBaris1;
            double lambdaMax_baris2 = (a12 + b22 + c32 + d42) * kriteria3_eigenBaris2;
            double lambdaMax_baris3 = (a13 + b23 + c33 + d43) * kriteria3_eigenBaris3;
            double lambdaMax_baris4 = (a14 + b24 + c34 + d44) * kriteria3_eigenBaris4;
            double totalLambdaMax = lambdaMax_baris1 + lambdaMax_baris2 + lambdaMax_baris3 + lambdaMax_baris4;
            
            CIk3 = (totalLambdaMax-4) / 3.0;
            CRk3 = CIk3 / 0.9;
            if(CRk3 <= 0.1000)
            {
                syarat[0] = "Kriteria";
                syarat[1] = "Judgement kriteria dapat diterima";
                syarat[2] = kriteria3_eigenBaris1 + "";
                syarat[3] = kriteria3_eigenBaris2 + "";
                syarat[4] = kriteria3_eigenBaris3 + "";
                syarat[5] = kriteria3_eigenBaris4 + "";
            }
            else if (CRk3 > 0.1000)
            {
                syarat[0] = "Berhenti di iterasi 3";
                syarat[1] = "Judgement kriteria tidak dapat diterima";
                syarat[2] = 0 + "";
                syarat[3] = 0 + "";
                syarat[4] = 0 + "";
                syarat[5] = 0 + "";
            }
        }
        else if(selisihNormalisasi2_baris1 <= 0.0100 &&
                selisihNormalisasi2_baris2 <= 0.0100 &&
                selisihNormalisasi2_baris3 <= 0.0100 &&
                selisihNormalisasi2_baris4 <= 0.0100)
        {
            double lambdaMax_baris1 = (a11 + b21 + c31 + d41) * kriteria2_eigenBaris1;
            double lambdaMax_baris2 = (a12 + b22 + c32 + d42) * kriteria2_eigenBaris2;
            double lambdaMax_baris3 = (a13 + b23 + c33 + d43) * kriteria2_eigenBaris3;
            double lambdaMax_baris4 = (a14 + b24 + c34 + d44) * kriteria2_eigenBaris4;
            double totalLambdaMax = lambdaMax_baris1 + lambdaMax_baris2 + lambdaMax_baris3 + lambdaMax_baris4;
            
            CIk2 = (totalLambdaMax-4) / 3.0;
            CRk2 = CIk2 / 0.9;
            if(CRk2 <= 0.1000)
            {
                syarat[0] = "Kriteria";
                syarat[1] = "Judgement kriteria dapat diterima";
                syarat[2] = kriteria2_eigenBaris1 + "";
                syarat[3] = kriteria2_eigenBaris2 + "";
                syarat[4] = kriteria2_eigenBaris3 + "";
                syarat[5] = kriteria2_eigenBaris4 + "";
            }
            else if (CRk2 > 0.1000)
            {
                syarat[0] = "Berhenti di iterasi 2";
                syarat[1] = "Judgement kriteria tidak dapat diterima";
                syarat[2] = 0 + "";
                syarat[3] = 0 + "";
                syarat[4] = 0 + "";
                syarat[5] = 0 + "";
            }
        }
        
        return syarat;
    }
    
    public double[][] tampilKriteria2()
    {
        double[][] tampilK1 = new double[4][4];
        tampilK1[0][0] = kriteria2_11;
        tampilK1[0][1] = kriteria2_12;
        tampilK1[0][2] = kriteria2_13;
        tampilK1[0][3] = kriteria2_14;
        tampilK1[1][0] = kriteria2_21;
        tampilK1[1][1] = kriteria2_22;
        tampilK1[1][2] = kriteria2_23;
        tampilK1[1][3] = kriteria2_24;
        tampilK1[2][0] = kriteria2_31;
        tampilK1[2][1] = kriteria2_32;
        tampilK1[2][2] = kriteria2_33;
        tampilK1[2][3] = kriteria2_34;
        tampilK1[3][0] = kriteria2_41;
        tampilK1[3][1] = kriteria2_42;
        tampilK1[3][2] = kriteria2_43;
        tampilK1[3][3] = kriteria2_44;
        
        return tampilK1;
    }
    public double[] tampilJumBarisK2()
    {
        double[] a = new double[4];
        a[0] = kriteria2_jumlahBaris1;
        a[1] = kriteria2_jumlahBaris2;
        a[2] = kriteria2_jumlahBaris3;
        a[3] = kriteria2_jumlahBaris4;
        return a;
    }
    public double[] tampilNormK2()
    {
        double[] a = new double[4];
        a[0] = kriteria2_eigenBaris1;
        a[1] = kriteria2_eigenBaris2;
        a[2] = kriteria2_eigenBaris3;
        a[3] = kriteria2_eigenBaris4;
        return a;
    }
    public double[][] tampilKriteria3()
    {
        double[][] tampilK1 = new double[4][4];
        tampilK1[0][0] = kriteria3_11;
        tampilK1[0][1] = kriteria3_12;
        tampilK1[0][2] = kriteria3_13;
        tampilK1[0][3] = kriteria3_14;
        tampilK1[1][0] = kriteria3_21;
        tampilK1[1][1] = kriteria3_22;
        tampilK1[1][2] = kriteria3_23;
        tampilK1[1][3] = kriteria3_24;
        tampilK1[2][0] = kriteria3_31;
        tampilK1[2][1] = kriteria3_32;
        tampilK1[2][2] = kriteria3_33;
        tampilK1[2][3] = kriteria3_34;
        tampilK1[3][0] = kriteria3_41;
        tampilK1[3][1] = kriteria3_42;
        tampilK1[3][2] = kriteria3_43;
        tampilK1[3][3] = kriteria3_44;
        
        return tampilK1;
    }
    public double[] tampilJumBarisK3()
    {
        double[] a = new double[4];
        a[0] = kriteria3_jumlahBaris1;
        a[1] = kriteria3_jumlahBaris2;
        a[2] = kriteria3_jumlahBaris3;
        a[3] = kriteria3_jumlahBaris4;
        return a;
    }
    public double[] tampilNormK3()
    {
        double[] a = new double[4];
        a[0] = kriteria3_eigenBaris1;
        a[1] = kriteria3_eigenBaris2;
        a[2] = kriteria3_eigenBaris3;
        a[3] = kriteria3_eigenBaris4;
        return a;
    }
    public double[] tampilCICRKriteria()
    {
        double[] a = new double[4];
        a[0] = CIk2;
        a[1] = CRk2;
        a[2] = CIk3;
        a[3] = CRk3;
        return a;
    }
    
    public double[] finishing(double kritA, double kritB, double kritC, double kritD,
                              double normXA, double normXB, double normXC, double normXD,
                              double normYA, double normYB, double normYC, double normYD,
                              double normZA, double normZB, double normZC, double normZD)
    {
        double[] finish = new double [3];
        finish[0] = (kritA*normXA) + (kritB*normXB) + (kritC*normXC) + (kritD*normXD);
        finish[1] = (kritA*normYA) + (kritB*normYB) + (kritC*normYC) + (kritD*normYD);
        finish[2] = (kritA*normZA) + (kritB*normZB) + (kritC*normZC) + (kritD*normZD);
        return finish;
    }

    /**
     * @return the x11
     */
    public double getX11() {
        return x11;
    }

    /**
     * @param x11 the x11 to set
     */
    public void setX11(double x11) {
        this.x11 = x11;
    }

    /**
     * @return the x12
     */
    public double getX12() {
        return x12;
    }

    /**
     * @param x12 the x12 to set
     */
    public void setX12(double x12) {
        this.x12 = x12;
    }

    /**
     * @return the x13
     */
    public double getX13() {
        return x13;
    }

    /**
     * @param x13 the x13 to set
     */
    public void setX13(double x13) {
        this.x13 = x13;
    }

    /**
     * @return the y21
     */
    public double getY21() {
        return y21;
    }

    /**
     * @param y21 the y21 to set
     */
    public void setY21(double y21) {
        this.y21 = y21;
    }

    /**
     * @return the y22
     */
    public double getY22() {
        return y22;
    }

    /**
     * @param y22 the y22 to set
     */
    public void setY22(double y22) {
        this.y22 = y22;
    }

    /**
     * @return the y23
     */
    public double getY23() {
        return y23;
    }

    /**
     * @param y23 the y23 to set
     */
    public void setY23(double y23) {
        this.y23 = y23;
    }

    /**
     * @return the z31
     */
    public double getZ31() {
        return z31;
    }

    /**
     * @param z31 the z31 to set
     */
    public void setZ31(double z31) {
        this.z31 = z31;
    }

    /**
     * @return the z32
     */
    public double getZ32() {
        return z32;
    }

    /**
     * @param z32 the z32 to set
     */
    public void setZ32(double z32) {
        this.z32 = z32;
    }

    /**
     * @return the z33
     */
    public double getZ33() {
        return z33;
    }

    /**
     * @param z33 the z33 to set
     */
    public void setZ33(double z33) {
        this.z33 = z33;
    }
    
}
